from django.urls import path
from . import views
from forum_app.views import index

urlpatterns = [
    path('register/', views.registerPage, name='register'),
    path('login/', views.loginPage, name='login'),
    path('home/', index, name='home'),
    path('logout/', views.logoutUser, name = 'logout'),
]


