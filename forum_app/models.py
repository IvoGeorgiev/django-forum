from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django import forms

class Category(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()

class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['name', 'description']

class Topic(models.Model):
    title = models.CharField(max_length=255)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, default = 1)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class Reply(models.Model):
    content = models.TextField()
    created_at = models.DateTimeField(default=timezone.now)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE, default= 1)
    user = models.ForeignKey(User, on_delete=models.CASCADE, default = 1)
    upvotes = models.IntegerField(default=0)
    downvotes = models.IntegerField(default=0)

class Vote(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    reply = models.ForeignKey(Reply, on_delete=models.CASCADE)
    upvote = models.BooleanField()