from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required, user_passes_test
from .models import Category, Topic, Reply, CategoryForm, Vote
from django.http import HttpResponse
from django.views.decorators.http import require_POST
from django.urls import reverse

from django.views.decorators.csrf import csrf_exempt   #REMOVE AFTER PORDUCTION


def index(request):
    categories = Category.objects.all()
    return render(request, 'forum_app/index.html', {'categories': categories})

@user_passes_test(lambda u: u.is_superuser)
def admin_panel(request):
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('admin_panel'))
    categories = Category.objects.all()
    topics = Topic.objects.all()
    replies = Reply.objects.all()
    form = CategoryForm()
    return render(request, 'forum_app/admin_panel.html', {'categories': categories, 'topics': topics, 'replies': replies, 'form': form})

def category(request, category_id):
    category = Category.objects.get(id=category_id)
    topics = category.topic_set.all()
    return render(request, 'forum_app/category.html', {'category': category, 'topics': topics})

@user_passes_test(lambda u: u.is_superuser)
def new_category(request):
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('admin_panel'))
    else:
        form = CategoryForm()
    return render(request, 'admin_panel.html', {'form': form})

@user_passes_test(lambda u: u.is_superuser)
@require_POST
def delete_category(request, category_id):
    category = get_object_or_404(Category, id=category_id)
    category.delete()
    return redirect(reverse('admin_panel'))

def topic(request, topic_id):
    topic = Topic.objects.get(id=topic_id)
    replies = topic.reply_set.all()
    return render(request, 'forum_app/topic.html', {'topic': topic, 'replies': replies})

@login_required
def new_topic(request, category_id):
    categories = Category.objects.all()
    if request.method == 'POST':
        category = get_object_or_404(Category, id=category_id)
        name = request.POST.get('name')
        content = request.POST.get('content')
        topic = Topic.objects.create(title=name, content=content, category=category, user=request.user)
        return redirect('topic', topic.id)
    return render(request, 'forum_app/new_topic.html', {'categories': categories})

@user_passes_test(lambda u: u.is_superuser)
@require_POST
def delete_topic(request, topic_id):
    topic = get_object_or_404(Topic, id=topic_id)
    topic.delete()
    return redirect(reverse('admin_panel'))

@login_required
def add_reply(request, topic_id):
    topic = get_object_or_404(Topic, id=topic_id)
    if request.method == 'POST':
        content = request.POST.get('content')
        reply = Reply.objects.create(content=content, topic=topic, user=request.user)
        reply.save()
        return redirect('topic', topic_id)
    return redirect('topic', topic_id)

@user_passes_test(lambda u: u.is_superuser)
@require_POST
def delete_reply(request, reply_id):
    reply = get_object_or_404(Reply, id=reply_id)
    reply.delete()
    return redirect(reverse('admin_panel'))


@login_required
def upvote(request, reply_id):
    reply = get_object_or_404(Reply, pk=reply_id)
    vote, created = Vote.objects.get_or_create(user=request.user, reply=reply, defaults={'upvote': True})
    if not created:
        return redirect('topic', topic_id=reply.topic.id)
    reply.upvotes += 1
    reply.save()
    return redirect('topic', topic_id=reply.topic.id)

@login_required
def downvote(request, reply_id):
    reply = get_object_or_404(Reply, pk=reply_id)
    vote, created = Vote.objects.get_or_create(user=request.user, reply=reply, defaults={'upvote': False})
    if not created:
        return redirect('topic', topic_id=reply.topic.id)
    reply.downvotes += 1
    reply.save()
    return redirect('topic', topic_id=reply.topic.id)

#just1
