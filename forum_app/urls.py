
from django.urls import path
from .views import index, category, topic, add_reply, new_category, new_topic, delete_category, admin_panel, delete_topic, delete_reply, upvote, downvote


urlpatterns = [
    path('', index, name='index'),
    path('admin-panel/', admin_panel, name='admin_panel'),
    path('category/<int:category_id>/', category, name='category'),
    path('category/new/', new_category, name='new_category'),
    path('category/<int:category_id>/new_topic/', new_topic, name='new_topic'),
    path('category/<int:category_id>/delete/', delete_category, name='delete_category'),
    path('topic/<int:topic_id>/', topic, name='topic'),
    path('topic/<int:topic_id>/new_reply/', add_reply, name='new_reply'),
    path('topic/<int:topic_id>/delete/', delete_topic, name='delete_topic'),
    path('reply/<int:reply_id>/delete/', delete_reply, name='delete_reply'),
    path('topic/upvote/<int:reply_id>/', upvote, name='upvote'),
    path('topic/downvote/<int:reply_id>/', downvote, name='downvote'),
]